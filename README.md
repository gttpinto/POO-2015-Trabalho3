
# Programacao Orientada a Objeto - Trabalho 3
 
* 7656209 - Guilherme Tomaz Torres Pinto
* 7986879 - Otavio Guido Silva
* 8066395 - Rodrigo das Neves Bernardi
 
 

Java Project -> Biblioteca



== ORGANIZACAO DA PASTA

No diretorio Biblioteca/ ha um diretorio src/ que contem os pacotes java com os arquivos .java, 
um diretorio build/ que contem os arquivos .class, um diretorio dist/ com um arquivo Biblioteca.jar



== PARA COMPILAR

O projeto foi criado no NetBeans e pode ser compilado por la. Se preferir, há um Makefile na raiz do projeto
que pode ser usado para compilar atravez do comando "make". Os parametros de entrada estão explicados no
item "== PARA RODAR".



== PARA RODAR

Para rodar, basta acessar o diretorio do arquivo .jar, digitar o comando "java -jar Biblioteca.jar <parametros>", 
onde <parametros> é a data em que o sistema deve abrir e deve ser digitada no formato 
"java -jar Biblioteca.jar dd mm aaaa".
Caso nenhum parametro seja passado, a data do SO será a data padrão.