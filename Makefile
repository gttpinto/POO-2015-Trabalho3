JAVA = src/biblioteca/*.java src/cadastros/*.java src/funcoes/*.java src/livros/*.java src/pessoas/*.java
CLASS = build/classes/biblioteca/*.class build/classes/cadastros/*.class build/classes/funcoes/*.class build/classes/livros/*.class build/classes/pessoas/*.class

all: Compile


Compile:clean
	javac -cp build -d build $(JAVA)

clean: 
	rm -rf build	
	mkdir build

code:
	subl $(JAVA)

zip:
	rm -rf Biblioteca.zip
	rm -rf Biblioteca
	mkdir Biblioteca
	cp *.txt Biblioteca
	cp -rf src/ Biblioteca
	cp -rf build/ Biblioteca
	cp Makefile Biblioteca
	zip -r Biblioteca.zip Biblioteca/
	rm -rf Biblioteca

run:
	java -jar dist/Biblioteca.jar #<parametros>
	#java -cp build classes.biblioteca.Biblioteca #<parametros>
